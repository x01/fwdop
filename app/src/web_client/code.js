"use strict";

function $(id) {
	if (id.charCodeAt(0)==35) return document.getElementById(id.substr(1));
	else if (id.charCodeAt(0)==46) return document.getElementsByClassName(id.substr(1));
	else return document.getElementsByTagName(id);
}

function RemoteStateError(msg) {
	Error.call(this, msg);
	this.message=msg;
}
RemoteStateError.prototype=Object.create(Error.prototype);
RemoteStateError.prototype.name='RemoteStateError';
Object.defineProperty( RemoteStateError.prototype, 'constructor', { value: RemoteStateError, enumerable: false, writable: true } );

function Conn(ws) {
	this._ws=ws;
}

Conn.open = function(url) {
	return new Promise((fulfill,reject) => {
		console.log(`opening a connection`);
		let ws=new WebSocket(url);
		let conn=new Conn(ws);
		ws.onopen = (ev) => { conn._onopen(ev); fulfill(conn); };
		ws.onmessage = (ev) => { conn._onmessage(ev); };
		conn._fwd_onerror = (ev) => {
			reject("failed to open a connection");
		};
		ws.onerror = (ev) => { conn._onerror(ev); }
		conn._fwd_onclose = (ev) => {
			reject("closed the connection");
		};
		ws.onclose = (ev) => { conn._onclose(ev); };
	});
}

Conn.prototype._onopen = function(ev) {
	console.log(" * onopen");
	this._open=true;
}

Conn.prototype._onmessage = function(ev) {
	console.assert(this._open, "onmessage: socket must be open");
	this?._fwd_onmessage(ev);
}

Conn.prototype._onerror = function(ev) {
	console.log(" * onerror");
	this._open=false; // --????
	this?._fwd_onerror(ev);
}

Conn.prototype._onclose = function(ev) {
	console.log(" * onclose");
	//console.assert(this._open, "onclose: socket must be open");
	this.onclose?.();
	this._fwd_onclose?.(ev);
	this._open=false;
}

Conn.prototype.send = function(data) {
	console.assert(this._open, "send: socket must be open");
	return new Promise((fulfill, reject) => {
		this._fwd_onerror = (ev) => { reject(new Error("Failed to send data")); };
		this._fwd_onclose = (ev) => { reject(new Error("Closed")); };
		this._ws.send(data);
		setTimeout(() => { fulfill(this); }, 0);
	});
}

Conn.prototype.recv = function() {
	console.assert(this._open, "recv: socket must be open");
	return new Promise ((fulfill, reject) => {
		this._fwd_onerror = (ev) => { reject(new Error("Failed to recv data")); };
		this._fwd_onclose = (ev) => { reject(new Error("Closed")); };
		this._fwd_onmessage = (ev) => { fulfill({conn:this, data:ev.data}); }
	});
}

Conn.prototype.close = function () {
	console.assert(this._open, "close: socket must be open");
	return new Promise ((fulfill, reject) => {
		console.log("- closing connection (Conn.close called)");
		this._ws.close();
		this._fwd_onerror = (ev) => {
			reject(new Error(" * onerror during close"));
			this._fwd_onmessage = this._fwd_onerror = this._fwd_onclose = null;
		};
		this._fwd_onclose = (ev) => {
			console.log(" * _fwd_onclose in response to close");
			this._fwd_onmessage = this._fwd_onerror = this._fwd_onclose = null;
			fulfill(ev);
		};
	});
}

function Client(poll_checkbox, code_input) {
	let self=this;
	this.ws=null;
	this.state=Client.CONN_NOT_CONNECTED;
	if (poll_checkbox.checked=this.get_polling())
	 	this.do_poll();
	if (code_input.value)
		this.update_accesscode(code_input.value);
	else if (window.localStorage.getItem('site.x01.access_code')) {
		this._access_code=window.localStorage.getItem('site.x01.access_code');
		code_input.value=this._access_code;
	}	
	
	this.progel=$('#info-progress');
	this._was_denied=false;
}

Client.prototype.on_load = function() {
	let self=this;
	// window.onfocus = function () { self.onfocus(); };
	// window.onblur = function () { self.onblur(); };
	// this.do_ping();
	// this.establish_connection().then((conn) => {});
}


Client.CONN_NOT_CONNECTED=0;
Client.CONN_CONNECTED=1;
Client.CONN_CONNECTING=2;

Client.REMOTE_UNKNOWN=0; // ⚛ ?
Client.REMOTE_IDLE=1; // ☀
Client.REMOTE_BUSY=2; // ⚒ ☎ ☏
Client.REMOTE_ERROR=3; // ☠

Client.KEEP_ALIVE_INTERVAL=3000;
Client.POLL_INTERVAL=500;

Client.is_focused = function() {
	return document.hasFocus();// || document.getElementById('iframe').contentWindow.document.hasFocus();
}
Client.prototype.set_polling = function(poll) {
	window.localStorage.setItem('site.x01.polling', poll);
	this._was_denied=false;
	if (poll)
		this.do_poll();
}
Client.prototype.get_polling = function() {
	let ps=window.localStorage.getItem('site.x01.polling');
	if (ps == null)
		window.localStorage.setItem('site.x01.polling', ps="true");
	return (ps.toLowerCase() == 'true');
}
Client.prototype.update_accesscode = function(code) {
	if (this._access_code != code)
		this._was_denied=false;
	this._access_code=code;
	window.localStorage.setItem('site.x01.access_code', this._access_code);
}

Client.prototype.set_status = function(state, remote_state=undefined) {
	this.state=state;
	let el=$('#info-text');
	let iel=$('#info-icon-img');
	if (!this._status_dict) this._status_dict={0:"disconnected", 1:"connected", 2:"connecting", 3:"remote error"};
	let set='bird';
	if (!this._remoteimg_dict)
		this._remoteimg_dict={
			undefined:`unknown.png`,
			'idle':"idle.png",
			'calling':"calling_open",
			'ringing':"ringing.png",
			'permissions':"denied.png"
		};

	let st_str='';
	let ico=`icons/${set}/${this._remoteimg_dict[remote_state]}`;
	if (remote_state==undefined)
		st_str=this._status_dict[state];
	else
		st_str=`${this._status_dict[state]} / ${remote_state}`;

	if (this._past_status != st_str) {
        this._past_status=st_str;
		if (remote_state=='calling' || remote_state=='busy')
			$('#open-button')?.classList.add('open-button-disabled');
		else
			$('#open-button')?.classList.remove('open-button-disabled');
		el.innerHTML=this._past_status=st_str;
		iel.src=ico;
	}
}

Client.prototype.disp_remote = function(remote_state) {
}

Client.prototype.establish_connection = function() {
	let url=`wss://${location.host==""?"localhost:8443":location.host}/gate`;
	// wait until previous command has finished
	var p1;
	if (!this._cc) {
		p1=Conn.open(url)
			.then(
				(conn) => {
					console.log(`connected to ${url}`);
					conn.onclose=() => { this.onconnclosed(conn); };
					this._cc=conn;
					return conn;
				},
				(reason) => {
					console.log(`failed to connect to ${url}. ${reason}`);
					this._cc = null;
					return Promise.reject(new Error(`Failed to connect to ${url}. ${reason}`));
				});
	} else
		p1=Promise.resolve(this._cc);

	if (this._inflight)
		return this._inflight=Promise.all([this._inflight]).then(() => p1);//(r) => { return p1; });
	else
		return this._inflight=p1;
}

Client.prototype.onconnclosed = function(conn) {
	console.log(" * connection was closed");
	this.set_status(Client.CONN_NOT_CONNECTED);
	this._cc=null;
}

Client.prototype.get_remote_status = function(conn) { // -> {conn, status}
	return new Promise((fulfill, reject) => {
		if (!this._access_code)
			reject(new RemoteStateError(`access code missing`));
		const cmds=JSON.stringify({op:'report', access:this._access_code});
		conn.send(cmds)
			.then((conn) => { return conn.recv(); })
			.then(({conn,data}) => {
				do_log(" - received: " + data + "<br>");
				let r=JSON.parse(data);
				console.log(` - remote is ${r.status}`);
				if (['idle', 'busy', 'calling', 'denied'].includes(r.status)) {
//					console.log(` - fulfill get_remote_status with ${r.status}`);
					fulfill({conn:conn, status:r.status});
				} else {
//					console.log(` - failing get_remote_status with 'invalid'`);
					return Promise.reject(new RemoteStateError(`unknown remote status '${r.status}'`));
				}
			})
			.catch((reason) => {
//				console.log(` - get_remote_status (in .catch) rejecting. ${typeof reason} - ${reason.message}`);
				this.set_status(Client.CONN_NOT_CONNECTED);
				reject(reason);
			});
		});
}

Client.prototype.do_open = function(cmd) {
	console.log(`sending ${cmd}`);

	this.set_status(Client.CONN_CONNECTING);
	this._inflight=this.establish_connection()
		.then((conn) => {

			console.log(` - asking for status`);
			return this.get_remote_status(conn); 
		})
		.then(({conn, status}) => {
			console.log(` - status got: ${status}`);
			if (status != 'idle') {
				if (status=='denied')
					this._was_denied=true;
				else
					this._was_denied=false;
				return Promise.reject(new RemoteStateError(`can't open right now as remote status is ${status}`));
			} else
				return Promise.resolve(conn);
		})
		.then((conn) => {
			console.log(` > asking to open`);
			this.set_status(Client.CONN_CONNECTED);
			return conn.send(JSON.stringify({op:'open', access:this._access_code}));
		})
		.then((conn) => { return conn.recv(); })
		.then(({conn,data}) => {
			try {
				do_log("received: " + data + "<br>");
				let r=JSON.parse(data);

				if (r.status) {
					switch(r.status) {
						case "pong": // unexpected
							break;
						case "idle": break;
						case "busy": break;
						case "calling": break;
						case "denied": break;
					}
				}
			} catch(err) {
				return Promise.reject(new RemoteStateError(`error parsing remote response. ${err.message}`));
			}
		})
		.then(() =>
		{
			this._inflight=null;
		})
		.catch((reason) => {
			//console.log(`got exception ${reason} (${reason.message})`);
			this._inflight=null;
			if (reason instanceof RemoteStateError) {
				console.log(`failed to execute 'open' command. ${reason}`);
				this.set_status(Client.REMOTE_ERROR);
			} else {
				console.log(`failed to execute command, closing connection ${reason}`);
				this.set_status(Client.CONN_NOT_CONNECTED);
				if (this._cc) this._cc.close();
				this._cc = null;
			}
		});
}

Client.prototype.do_poll = function() {
	this._last_poll=this._last_poll || -Client.POLL_INTERVAL;	
	let polling=false;
	let old_st=undefined;
	let poll_func=() => {

		console.log("do log");

		let poll_later=() => {
			setTimeout( () => {
				if (this.get_polling() && !polling)
					poll_func();
				else
					this.set_status(Client.CONN_CONNECTED);
			}, Client.POLL_INTERVAL );
		};

		if (!Client.is_focused() || this._was_denied) {
			poll_later();
			return;
		}
	
		polling=true;
		this._inflight=this.establish_connection()
			.then((conn) => {
				this.set_status(Client.CONN_CONNECTED, old_st);
				return this.get_remote_status(conn);
			})
			.then(({conn, status}) => {
				this.set_status(Client.CONN_CONNECTED, old_st=status);
				poll_later();
				polling=false;
			})
			.then(() => { this._inflight=null; })
			.catch((reason) => {
				old_st=undefined;
				do_log(reason.message);
				this.set_status(Client.CONN_NOT_CONNECTED);
				poll_later();
				polling=false;
				this._inflight=null;
			});
	};
	if (Date.now() - this._last_poll >= Client.POLL_INTERVAL && !polling && this.get_polling()) {
		this._last_poll=Date.now();
		poll_func();
	}
}

function do_log(s) {
	// let l=$('#log');
	// l.innerHTML += s;
}

function WebSocketTest() {
	if ("WebSocket" in window) {
	   //alert("WebSocket is supported by your Browser!");

	   // Let us open a web socket
	   let ws = new WebSocket("wss://localhost:8080/echo");
	} else {

	   // The browser doesn't support WebSocket
	   alert("WebSocket NOT supported by your Browser!");
	}
 }
