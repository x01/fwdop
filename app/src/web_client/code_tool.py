#!env python3
import sys
import random
import doctest
import json

#      0       8       10      18      20
xform="0123456789abcdefghijklmnpqrstuvwxyz";
bits=1
def to_str(n):
    """ rencode number
    >>> to_str(0)
    0
    >>> to_str(32)
    10
    """

    if n==0: return "0"

    s=""
    rad=len(xform)
    while (n!=0):
        i=n%rad
        s=xform[i]+s
        n//=rad

    return s

def to_num(s):
    """ rencode number
    >>> to_num(15)
    40
    """

    r=0
    rad=len(xform)
    m=0
    for c in reversed(s):
        i=xform.index(c)
        if i==-1: return 0
        r += rad**m * i
        m=m+1

    return r

p=1
x=0
def generate_num():
    m=(1<<bits)-1
    r=random.getrandbits(bits)
    return (r * p) ^ (x&m)

def test_num(c):
    m=(1<<bits)-1
    good=((c ^ (x&m)) % p == 0)
    return good


def print_code(code):
    s=to_str(code)
    n=to_num(s)

    #print("code: {0} / {1} - decoded:{2} is {3}".format(code, s, n, "good" if test_num(n) else "invalid"))
    print(s)

#print("radix: {0}".format(len(xform)))
try:
    with open('../../../../config.json') as f:
        data = json.load(f)
        print("parsing config. num:{0} xor:{1} bits:{2}".format(data['c_num'], data['c_xor'], data['c_bits']))
        p=int(data['c_num'], 0)
        x=int(data['c_xor'], 0)
        bits=int(data['c_bits'], 0)
except:
    print("config not loaded, prime:1, xor:0")

if len(sys.argv) > 1:
    for code in sys.argv[1:]:
        print("{0} {1} {2}".format(code, to_num(code), "OK" if test_num(to_num(code)) else "INCORRECT"))
else:
    random.seed()
    n=generate_num()
    print_code(n)
    if not test_num(n):
        print("FAIL, code doesn't pass")

