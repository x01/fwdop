package com.valdemar.fwdop_a9_Pie;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.renderscript.ScriptGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.*;
import java.net.*;
import java.security.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLServerSocketFactory;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoWSD;

interface WebInteract_CallController {
    MainActivity.AppState getState();
    boolean attemptCall();
}

interface WebInteract_PushState {
    void pushState(MainActivity.AppState state);
}

abstract class WebInteract_Common extends NanoWSD {

    protected MainActivity.Logger m_logger;

    WebInteract_Common(int port, MainActivity.Logger logger) {
        super(port);
        m_logger = logger;
    }

    SSLServerSocketFactory createSSLFactory(AssetManager ass, String keyFile, String password)
            throws IOException, GeneralSecurityException {
        InputStream keystoreStream = ass.open("cert.p12");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(keystoreStream, password.toCharArray());

        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, password.toCharArray());

        return NanoHTTPD.makeSSLSocketFactory(keyStore, keyManagerFactory);
    }

    InetAddress getOutterIp() throws IOException {
        InetAddress ip = null;
//        try {
            for (Enumeration<NetworkInterface> ii = NetworkInterface.getNetworkInterfaces(); ii.hasMoreElements(); ) {
                NetworkInterface i=ii.nextElement();
                if (i.isVirtual()) continue;
                if (i.isLoopback()) continue;
                if (!i.getName().startsWith("wlan")) continue;

                StringBuilder addr= new StringBuilder();
//                for(Enumeration<InetAddress> ai=i.getInetAddresses(); ai.hasMoreElements(); ) {
//                    InetAddress a=ai.nextElement();
//                    addr.append(a.getHostName()).append(" / ").append(a.getAddress()).append(" ");
//                }
                for(Iterator<InterfaceAddress> ai = i.getInterfaceAddresses().listIterator(); ai.hasNext(); ) {
                    InterfaceAddress a=ai.next();
                    addr.append(a.getAddress()).append(" ");
                }
//                m_logger.addLog("Interface: " + i.getName() + " - " + addr + "\n");
            }

            ip = InetAddress.getLocalHost();
//        } catch(IOException e) {
//            m_logger.addLog("Failed to get host/name. " + e.getMessage());
//        }

        return ip;
    }

    static String guessMimeType(String filename, InputStream is) {
        String ext = "";
        int dot = filename.lastIndexOf('.');
        if (dot >= 0)
            ext = filename.substring(dot + 1);

        switch (ext) {
            case "txt":
                return "text/plain";
            case "html":
            case "htm":
                return "text/html";
            case "js":
                return "text/javascript";
            case "ico":
                return "image/vnd.microsoft.icon";
            case "json":
                return "application/json";
            case "png":
                return "image/png";
            case "jpg":
            case "jpeg":
                return "image/jpeg";
            case "woff":
                return "font/woff";
            case "woff2":
                return "font/woff2";
            default:
                return "application/octet-stream";
        }
//        try {
//            return URLConnection.guessContentTypeFromStream(is);
//        } catch(IOException e) {
//            return "application/octet-stream";
//      }
    }
}

class WebInteract_HostInfo
{
    WebInteract_HostInfo() { rejects=0; latestRejectTime=LocalDateTime.now(); }
    public int rejects;
    public LocalDateTime latestRejectTime;
}

class WebInteract_HttpRedirect extends NanoHTTPD {
    int m_httpsPort;
    MainActivity.Logger m_logger;
    public WebInteract_HttpRedirect(int httpPort, int securePort, Context ctx, MainActivity.Logger logger) throws IOException {
        super(httpPort);
        m_logger=logger;
        m_httpsPort=securePort;
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        m_logger.addLog("Http server started on " + this.getHostname() + ":" + this.getListeningPort() + "\n");
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        String h=session.getHeaders().get("h" +
                "ost");
        if (h != null) {
            int colon=h.lastIndexOf(':');
            if (colon != -1)
                h = h.substring(0, colon);
        }

        String newUri = "https://" + h + ":" + m_httpsPort + uri;

        NanoHTTPD.Response resp = newFixedLengthResponse(Response.Status.REDIRECT, "text/html", "Http not supported");
        resp.addHeader("Location", newUri);
        return resp;
    }
}

public class WebInteract extends WebInteract_Common implements WebInteract_PushState {
    AssetManager m_assetManager;
    WebInteract_CallController m_callController;
    Set<WsdSocket> m_openSockets = new HashSet<WsdSocket>();
    protected Map<String, WebInteract_HostInfo> m_dropped = new HashMap<String, WebInteract_HostInfo>();
    WebInteract_HttpRedirect m_httpRedirect;

    // return false if connection should be dropped
    public boolean processReject(String remote) {
        WebInteract_HostInfo info;
        if (m_dropped.containsKey(remote)) {
             info = m_dropped.get(remote);
        } else {
            info = new WebInteract_HostInfo();
            m_dropped.put(remote, info);
        }

        LocalDateTime tenMinAgo=LocalDateTime.now().minusMinutes(5);
        if (info.latestRejectTime.isBefore(tenMinAgo)) {
            info.rejects=0;
        } else {
            info.latestRejectTime = LocalDateTime.now();
            info.rejects++;
            if (info.rejects >= 10)
                return false;
        }

        if (m_dropped.size() > 1024)
            m_dropped.entrySet().removeIf(e -> e.getValue().latestRejectTime.isBefore(tenMinAgo));

        return true;
    }


    @Override
    public void pushState(MainActivity.AppState state) {
//        synchronized (m_openSockets) {
//            for (WebSocket ws : m_openSockets) {
//                String sts = state.name().toLowerCase();
//                try {
//                    ws.send("{\"status\":\"" + sts + "\"}");
//                } catch (IOException e) {
//
//                }
//            }
//        }
    }

    public WebInteract(int port, int securePort, Context ctx, WebInteract_CallController callController, MainActivity.Logger logger) throws IOException {
        super(securePort, logger);
        m_assetManager = ctx.getAssets();
        m_callController = callController;

        m_httpRedirect = new WebInteract_HttpRedirect(port, securePort, ctx, logger);

        String keyPassword="password";

        try {
            SSLServerSocketFactory sslServerSocketFactory = createSSLFactory(m_assetManager, "localhost.p12", keyPassword);
            makeSecure(sslServerSocketFactory, null);
        } catch(GeneralSecurityException e) {
            m_logger.addLog("Encountered a problem with certificate for https server. " + e.getMessage());
            return;
        }

        try {
            InetAddress addr = getOutterIp();
            m_logger.addLog("Host: " + addr.getHostName() + " / " + addr + "\n");
        } catch(IOException e) {
            m_logger.addLog("Failed to get host/name. " + e.getMessage());
        }

        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        m_logger.addLog("Https/wss server started on " + this.getHostname() + ":" + this.getListeningPort() + "\n");
    }

    @Override public Response serveHttp(IHTTPSession session) {
        long start = System.currentTimeMillis();
        String uri = session.getUri();
        String file = uri.substring(uri.indexOf('/')+1);

        m_logger.addLog(session.getMethod() + " " + file + "(" + guessMimeType(file, null) + ") to " + session.getRemoteIpAddress() + "\n");

        if (file == null || file.isEmpty())
            file="index.html";

        NanoHTTPD.Response resp = null;

        StringBuilder msg = new StringBuilder();

        if (file.length() > 0 && !file.equals("report")) {
            try {
                InputStream buffStream = m_assetManager.open(file);
                String mimeType = guessMimeType(file, buffStream);
                //resp = newChunkedResponse(Response.Status.OK, mimeType, buffStream);
                resp = newChunkedResponse(Response.Status.OK, mimeType, buffStream);
//                buffStream.close();
            } catch(IOException e) {
                resp = newFixedLengthResponse(Response.Status.NOT_FOUND, "text/plain", "File not found");
            }
        } else {
            msg.append("file: " + file + "<br>\n");
            msg.append("uri: " + session.getUri() + "<br>\n");
            msg.append("remote: " + session.getRemoteHostName() + "<br>\n");
            msg.append("params: " + session.getParameters() + "<br>\n");
            msg.append("headers: " + session.getHeaders() + "<br>\n");
            msg.append("-- generated in " + (System.currentTimeMillis() - start) + "ms<br>\n");
            resp = newFixedLengthResponse(msg + "</body></html>\n");
        }

        return resp;
    }

    @Override
    public void stop() {
        super.stop();
        m_httpRedirect.stop();
    }

    @Override
    protected WebSocket openWebSocket(IHTTPSession session) {
        String uri = session.getUri();
        String file = uri.substring(uri.lastIndexOf('/')+1);
        if (!Objects.equals(file, "gate"))
            return null;
        m_logger.addLog(session.getMethod() + " WebSocket " + file + " to " + session.getRemoteIpAddress() + "\n");
        return new WsdSocket(session);
    }

    private class WsdSocket extends WebSocket {
        public WsdSocket(IHTTPSession handshakeRequest) { super(handshakeRequest); }

        //override onOpen, onClose, onPong and onException methods
        @Override
        protected void onOpen() {
//            synchronized (m_openSockets) {
//                m_openSockets.add(this);
//            }
        }

        @Override
        protected void onClose(WebSocketFrame.CloseCode code, String reason, boolean initiatedByRemote) {
//            synchronized (m_openSockets) {
//                m_openSockets.remove(this);
//            }
        }

        @Override
        protected void onPong(WebSocketFrame pong) {
        }

        @Override
        protected void onException(IOException exception) {
        }

        // messages
        // 1. {op:"ping"} -> {status:"pong"}
        // 2. {op:"report"} -> {status:"calling"|"idle"|"busy"}
        // 3. {op:"open"} -> {status: ... }

        @Override
        protected void onMessage(WebSocketFrame frame) {
            try {
                JSONObject js = new JSONObject(frame.getTextPayload());
                //m_logger.addLog("onMessage " + frame.getOpCode() + "\n");

                IHTTPSession hs = this.getHandshakeRequest();
                boolean doDeny = false;
                if (!js.has("access"))
                    doDeny=true;

                String code=js.get("access").toString().toLowerCase();
                if (!AccessCode.is_valid(code)) {
                    String remote=hs.getRemoteIpAddress();
                    if (!processReject(remote)) {
                        m_logger.addLog("Too many retries, dropping " + hs.getRemoteIpAddress() + ".");
                        this.close(WebSocketFrame.CloseCode.NormalClosure, "Too many login attempts", false);
                        frame.setOpCode(WebSocketFrame.OpCode.Close);
                        return;
                    }
                    doDeny=true;
                }

                if (doDeny) {
                    m_logger.addLog("Bad access code from " + hs.getRemoteIpAddress() + ". Access denied.");
                    send("{\"status\":\"denied\"}");
                    return;
                }

                if (js.has("op")) {
                    MainActivity.AppState status = m_callController.getState();
                    String sts=status.name().toLowerCase();
                    switch (js.getString("op")) {
                        case "ping":
                            send("{\"status\":\"pong\"}");
                            break;
                        case "report":
                            send("{\"status\":\"" + sts + "\"}");
                            break;
                        case "open":
                            boolean call = status == MainActivity.AppState.Idle;
                            if (call)
                                call = m_callController.attemptCall();
                            send("{\"status\":\"" + (call ? "calling" : "busy") + "\"}");
                            break;
                    }
                }
            } catch(JSONException e) {
                // simply ignore if can't parse json
            } catch (IOException e) {
                // handle
            }
        }
    }

}
