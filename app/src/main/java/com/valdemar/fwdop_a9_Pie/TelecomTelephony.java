package com.valdemar.fwdop_a9_Pie;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;

public class TelecomTelephony implements TelephonyHelper {

    Activity m_activity;
    MainActivity.Logger m_logger;

    private TelecomManager m_telecomManager;
    private PermissionHelper m_permHelper;
    private TelephonyManager m_telephony;
    private long calledSession = 0;

    private static String LIFETIME_CALL_KEY="called-lifetime";

    void increaseLifetime() {
        SharedPreferences sharedPreferences = m_activity.getPreferences(Context.MODE_PRIVATE);
        long lifetimeCalled = sharedPreferences.getLong(LIFETIME_CALL_KEY, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        lifetimeCalled++;
        editor.putLong(LIFETIME_CALL_KEY, lifetimeCalled);
        editor.commit();
    }

    long getLifetime() {
        SharedPreferences sharedPreferences = m_activity.getPreferences(Context.MODE_PRIVATE);
        return sharedPreferences.getLong(LIFETIME_CALL_KEY, 0);
    }

    public TelecomTelephony(Activity activity, PermissionHelper permHelper, MainActivity.Logger logger) {
        m_permHelper = permHelper;
        m_activity = activity;
        m_logger = logger;
        m_telecomManager = (TelecomManager)activity.getSystemService(Context.TELECOM_SERVICE);
        m_telephony = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            String tel=m_telephony.getLine1Number();
            m_logger.addLog("This phone: " + tel + "\n");
        } catch(SecurityException e) {

        }
//        PhoneAccount acc = m_telecomManager.getConnectionManager();
//        m_telecomManager.registerPhoneAccount();
    }

    @Override
    public Stats getStats() {
        Stats s = new Stats();
        s.calledLifetime = getLifetime();
        s.calledSession = calledSession;
        return s;
    }

    @Override
    public State state() {
        try {
            int state=m_telephony.getCallState();
            if (state==TelephonyManager.CALL_STATE_IDLE)
                return State.IDLE;

            if (m_telecomManager.isInCall())
                return state==TelephonyManager.CALL_STATE_RINGING ? State.RINGING : State.IN_CALL;
            else if (state==TelephonyManager.CALL_STATE_OFFHOOK)
                return State.REMOTE_OFFHOOK;

            return State.IDLE;
        } catch (SecurityException e) {
            m_logger.addLog("Failed to access telephony status. " + e.toString() + "\n");
            return State.IDLE;
        }
    }

    @Override
    public boolean endCall() {
        try {
            return m_telecomManager.endCall();
        } catch(SecurityException e) {
            m_logger.addLog("Failed to end call. " + e.toString() + "\n");
            return false;
        }
    }

    @Override
    public boolean call(Uri number) {
        if (!m_permHelper.check(PermissionHelper.CALL)) {
            m_logger.addLog("CALL_PHONE permission not available. Restart the app and allow to make calls when asked.\n");
            return false;
        }

        try {
            m_telecomManager.placeCall(number, null);
            calledSession++;
            increaseLifetime();
        } catch(SecurityException e) {
            m_logger.addLog("Failed to place a call to " + number + ". " + e.toString() + "\n");
            return false;
        } catch(Exception e) {
            m_logger.addLog("Failed to place a call to " + number + ". " + e.toString() + "\n");
            return false;
        }

        return true;
    }

    @Override
    public boolean isOK() { return true; }
}
