package com.valdemar.fwdop_a9_Pie;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final static long BATT_K = 1; // set to 200 for debugging

    private final static long CHECK_BATTERY_PERIOD = 60 * 1000 / BATT_K;
    private final static long WARN_BATTERY_DELAY = 15 * 60 * 1000 / BATT_K;
    private final static long WARN_MESSAGE_PERIOD = 30 * 60 * 1000 / BATT_K;

    public enum AppState { Idle, Calling, Ringing, Permissions }

    public static class Config {
        String phone;
        long prime;
        long xor;
        long bits;
    }

    public class Logger
    {
        void displayState(AppState state) {
            if (Looper.getMainLooper().isCurrentThread())
                MainActivity.this.do_setState(state);
            else
                runOnUiThread(new Runnable() {
                    @Override public void run() { MainActivity.this.do_setState(state); }
                });
        }
        void addLog(final String s) {
            if (Looper.getMainLooper().isCurrentThread())
                MainActivity.this.do_addLog(s);
            else
                runOnUiThread(new Runnable() {
                    @Override public void run() { MainActivity.this.do_addLog(s); }
                });
        }
        void replaceLog(final String s) {
            if (Looper.getMainLooper().isCurrentThread())
                MainActivity.this.do_replaceLog(s);
            else
                runOnUiThread(new Runnable() {
                    @Override public void run() { MainActivity.this.do_replaceLog(s); }
                });
        }
//        void addLog(final String s1, final String s2) {
//            if (Looper.getMainLooper().isCurrentThread())
//                MainActivity.this.do_addLog(s1, s2);
//            else
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        MainActivity.this.do_addLog(s1, s2);
//                    }
//                });
//        }
    }

    int m_timeout = 18000; // in ms

    Logger m_logger = new Logger();
    TextView m_counterText;
    TextView m_statusText;
    TextView m_logText;
    StringBuilder m_logContent = new StringBuilder();

    void do_addLog(String s) {
        //System.out.println("LOG: " + s);
        if (m_logContent.length() > 256 * 20)
            m_logContent.delete(0, m_logContent.length() / 2);

        m_logContent.append(s);
        //m_logContent.append("\n");
        m_logText.setText(m_logContent);
    }
    void do_replaceLog(String s) {
        int start=m_logContent.length() - s.length();
        if (start < 0) start = 0;
        m_logContent.replace(start, m_logContent.length(), s);
        m_logText.setText(m_logContent);
    }
    void do_setState(AppState state) {
        String msg = "";
        switch (state) {
            case Idle:
                msg = "Idle";
                break;
            case Calling:
                msg = "Calling";
                break;
            case Ringing:
                msg = "Ringing";
                break;
            case Permissions:
                msg = "Requesting Permissions";
                break;
        }
        m_statusText.setText(msg);

        String counterText = "";
        if (m_telephony != null) {
            TelephonyHelper.Stats s = m_telephony.getStats();
            counterText = String.format("%d / %d", s.calledSession, s.calledLifetime);
        }
        m_counterText.setText(counterText);
    }
//    void do_addLog(String s1, String s2) {
//        m_logContent.append(s1).append(s2);
//        m_logContent.append("\n");
//        m_mainText.setText(m_logContent);
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        m_permHelper._onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    class Worker implements Runnable, WebInteract_CallController {

        boolean m_quit;

        Activity m_act;
        TelephonyHelper m_tel;
        MainActivity.Logger m_logger;
        MainActivity.Config m_config;
        PermissionHelper m_permHelper;

        IntentFilter m_ifilter;
        Intent m_batteryStatus;

        Worker(Activity act, PermissionHelper permHelper, MainActivity.Config config, TelephonyHelper tel, Logger logger) {
            m_permHelper = permHelper;
            m_act = act;
            m_tel = tel;
            m_logger = logger;
            m_config=config;
        }

        AppState m_currentState = AppState.Idle;
        boolean m_requestCall = false;

        private void setState(AppState state) {
            m_currentState = state;

            //System.out.println("fwd: m_currentState -> " + state);

            //m_logger.displayState(m_currentState);
            if (m_webInteract != null)
                m_webInteract.pushState(state);
        }

        long m_batteryLastCheck = -1;
        long m_batteryProblemDetected = -1;
        boolean m_batteryWarning = false;
        long m_warningSentTime = -1;
        boolean m_warningSent = false;

        long m_batteryOKSentTime = -1;
        boolean m_batteryOKSent = false;

        boolean checkBattery() {
            long now = System.currentTimeMillis();

            if (now - m_batteryLastCheck < CHECK_BATTERY_PERIOD)
                return true;

            m_batteryLastCheck = now;

            m_ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            m_batteryStatus = m_act.registerReceiver(null, m_ifilter);

            int status = m_batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int level = m_batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = m_batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float charge = level / (float)scale;
            if (!isCharging && charge < 0.75) {

                if (!m_batteryWarning) {
                    m_batteryWarning = true;
                    m_batteryProblemDetected = now;

                    m_logger.addLog(String.format(Locale.ROOT, "Battery PROBLEM detected. charging:%d, charge:%2.2f\n",
                            isCharging?1:0, charge * 100.0f));
                }

                if (m_batteryWarning && now - m_batteryProblemDetected >= MainActivity.WARN_BATTERY_DELAY)
                {
                    if (now - m_warningSentTime > MainActivity.WARN_MESSAGE_PERIOD) {
                        m_warningSent = true;
                        m_batteryOKSent = false;
                        m_warningSentTime = now;

                        // send warning
                        String msg = String.format(Locale.ROOT, "FwdOp Battery 🔋 Warning 😵. Battery level: %3d%%, charging: %s",
                                (int)(charge * 100.0f), isCharging?"yes":"no");

                        if (m_emergencyUrl != null && !m_emergencyUrl.isEmpty())
                            sendMessage(m_emergencyUrl, msg);
                    }
                }

                return false;

            } else {
                if (m_batteryWarning)
                    m_logger.addLog(String.format(Locale.ROOT, "Battery OK. charging:%d charge:%2.2f\n",
                            isCharging?1:0, charge * 100.0f));

                if (m_warningSent && !m_batteryOKSent) {
                    m_batteryOKSent=true;

                    String msg = String.format(Locale.ROOT, "FwdOp Battery OK ✌️. Battery level: %3d%%, charging: %s",
                            (int)(charge * 100.0f), isCharging?"yes":"no");

                    if (m_emergencyUrl != null && !m_emergencyUrl.isEmpty())
                        sendMessage(m_emergencyUrl, msg);
                }

                m_batteryWarning = false;
                m_warningSent = false;
                return true;
            }
        }

        private void sleep(long ms) {
            try {
                Thread.sleep(ms);
            } catch (InterruptedException e) {

            }
        }

        private void makeCall() {
            String fwdNumber = m_config.phone;
            m_tel.call(Uri.fromParts("tel", fwdNumber, null));
        }

        private void sendMessage(String urlFmt, String message) {

            try {
                String encoded = URLEncoder.encode(message, StandardCharsets.UTF_8.toString());
                String urlString = String.format(urlFmt, encoded);

                URL url = new URL(urlString);
                URLConnection conn = url.openConnection();

                StringBuilder sb = new StringBuilder();
                InputStream is = new BufferedInputStream(conn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String inputLine = "";
                while ((inputLine = br.readLine()) != null) {
                    sb.append(inputLine);
                }
                String response = sb.toString();
                m_logger.addLog("Message send.\n");

            } catch(Exception e) {
                m_logger.addLog("Failed to send message. " + e.getMessage() + "\n");
            }
        }

        @Override
        public void run() {

            long refresh = 500;
            long start = System.currentTimeMillis();
            boolean wroteWaiting = false;
            int index = 0;
            String progress = "⣾⣽⣻⢿⡿⣟⣯⣷"; // "▁▂▃▄▅▆▇█▇▆▅▄▃▁"

            while (!m_permHelper.gotResult()) {
                if (System.currentTimeMillis() - start > 10 * 1000) {
                    m_logger.addLog("Timed out while waiting for permission result, aborting");
                    return;
                }

                sleep(refresh);
                if (!wroteWaiting) {
                    m_logger.addLog("waiting... ");
                    wroteWaiting = true;
                }
                m_logger.replaceLog("" + progress.charAt(index));
                index = (index + 1) % progress.length();
            }

            try {
                if (m_permHelper.check(PermissionHelper.INTERNET)) {
                    m_webInteract = new WebInteract(8080, 8443, m_act, this, m_logger);
                } else
                    m_logger.addLog("Internet permission not granted, web interaction disabled.\n");
            } catch (IOException e) {
                m_logger.addLog("Failed to launch Web Service, web interaction will not be available. " + e.getMessage() + "\n");
            }

            TelephonyHelper.State prevState = TelephonyHelper.State.IDLE;
            long stateStart = System.currentTimeMillis();
            while (!m_quit) {

                TelephonyHelper.State currState = m_telephony.state();
                System.out.println("telephony state: " + m_telephony.state());

                // process state switch
                if (prevState != currState) {
                    if (currState == TelephonyHelper.State.RINGING) {
                        setState(AppState.Ringing);
                        m_logger.addLog("-> RINGING");

                        sleep(refresh);

                        m_logger.addLog(", END CALL");
                        m_tel.endCall();

                        m_logger.addLog(", CALLING \n");
                        makeCall();
                        setState(AppState.Calling);
                    } else if (currState == TelephonyHelper.State.IDLE) {
                        setState(AppState.Idle);
                        m_logger.addLog("-> IDLE\n");
                    } else if (currState == TelephonyHelper.State.IN_CALL) {
                        setState(AppState.Calling);
                        m_logger.addLog("-> IN CALL\n");
                    } else if (currState == TelephonyHelper.State.REMOTE_OFFHOOK) {
                        m_logger.addLog("-> REMOTE OFF HOOK\n");
                    }

                    stateStart = System.currentTimeMillis();
                    prevState = currState;
                }

                // process state itself
                m_logger.displayState(m_currentState);
                if (currState == TelephonyHelper.State.RINGING) {
                    // m_logger.displayState(AppState.Ringing);
                    if (System.currentTimeMillis() - stateStart > m_timeout) {
                        m_logger.addLog("Unexpectedly can't pickup in " + m_timeout + "ms, straight endingCall\n");
                        m_tel.endCall();
                    }
                } else if (currState == TelephonyHelper.State.IDLE) {
                    if (m_requestCall) {
                        m_requestCall = false;

                        m_logger.addLog("CALLING\n");
                        setState(AppState.Calling);
                        makeCall();
                    }
                    //m_logger.displayState(AppState.Idle);
                } else if (currState == TelephonyHelper.State.IN_CALL) {
                    //m_logger.displayState(AppState.Calling);
                    if (System.currentTimeMillis() - stateStart > m_timeout) {
                        m_logger.addLog("Unable to make a call in " + m_timeout + "ms. Aborting\n");
                        m_tel.endCall();
                        // m_logger.displayState(AppState.Idle);
                    }
                } else if (currState == TelephonyHelper.State.REMOTE_OFFHOOK) {
                    //m_logger.displayState(AppState.Idle);
                }

                sleep(refresh);

                checkBattery();
            }

            m_logger.addLog("Exiting worker thread\n");
            if (m_webInteract != null)
                m_webInteract.stop();
        }

        @Override
        public AppState getState() {
            return m_currentState;
        }

        @Override
        public boolean attemptCall() {
            if (m_currentState == AppState.Idle) {
                m_requestCall = true;
                return true;
            }
            return false;
        }
    }

    Worker m_worker;
    Thread m_workerThread;
    TelephonyHelper m_telephony;
    PermissionHelper m_permHelper;

    WebInteract m_webInteract;

    class KeepRunning {
        KeepRunning() {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//            m_wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                    "fwdop::MainWakeTag");
//            m_wakeLock.acquire();
        }
//        PowerManager.WakeLock m_wakeLock;
    }

    KeepRunning m_keepRunning;
    String m_emergencyUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tel);

        m_keepRunning = new KeepRunning();

        m_counterText = findViewById(R.id.textCounter);
        m_statusText = findViewById(R.id.textStatus);
        m_logText = findViewById(R.id.textLog);
        m_logText.setMovementMethod(new ScrollingMovementMethod());
        m_logger.addLog ("CALL OPERATOR IS ON!\n");
        m_logger.addLog ("(C) 2021 hardesk, timeout: " + m_timeout / 1000 + "\n");

        Config config=new Config();
        try {
            InputStream configFile = getAssets().open("config.json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(configFile));
            StringBuilder sb = new StringBuilder();
            String str;
            while((str = reader.readLine())!= null)
                sb.append(str);

            JSONObject configJS = new JSONObject(sb.toString());

            config.phone=configJS.getString("phone");
            config.prime=configJS.getLong("c_num");
            config.bits=configJS.getLong("c_bits");
            String h=configJS.getString("c_xor");
            m_emergencyUrl=configJS.optString("emergency_url"); // generates GET, %s is replaced with url-escaped text
            m_logger.addLog("emergency set to " + m_emergencyUrl);
            if(h.indexOf('x')!=-1) h=h.substring(h.indexOf('x')+1);
            config.xor=Integer.parseUnsignedInt(h, 16);

            m_logger.addLog("Relay to: " + config.phone + "\n");
            m_logger.addLog("Access code params: " + config.prime + "-" + String.format("0x%08x", config.xor)
                    + " bits:" + config.bits + "\n");
            AccessCode.init(config.prime, config.xor, config.bits);
        } catch(Exception e) {
            m_logger.addLog("Failed to load config, unable to continue. " + e.getMessage());
            return;
        }

        m_permHelper = new PermissionHelper(this, m_logger);
        m_permHelper.acquire();

        m_telephony = new TelecomTelephony(this, m_permHelper, m_logger);
        if (!m_telephony.isOK()) {
            m_logger.addLog("FATAL ERROR: can't continue without an obtained telephony service.\n");
            return;
        }

        m_permHelper.changeDialer();

        m_worker = new Worker(this, m_permHelper, config, m_telephony, m_logger);
        m_workerThread = new Thread(m_worker);
        m_workerThread.start();
        System.out.println("thread started\n");
    }

    public void onOpenClick(View view) {
        if (m_worker != null)
            m_worker.attemptCall();
    }

    @Override
    public void onDestroy () {

        m_logger.addLog("DESTROYING\n");

        m_worker.m_quit = true;

        try { m_workerThread.join(2000); } catch(InterruptedException e) {}

        super.onDestroy ();
    }

    @Override
    public void onStart() {
        super.onStart();
        m_logger.addLog("onStart\n");
    }

    @Override
    public void onStop() {
        super.onStop();
        m_logger.addLog("onStop\n");
    }

//    private static final int REQUEST_ID = 1;
//
//    public void requestRole() {
//        BIND_INCALL_SERVICE
//        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_CALL_SCREENING);
//        startActivityForResult(intent, REQUEST_ID);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_ID) {
//            if (resultCode == android.app.Activity.RESULT_OK) {
//                // Your app is now the call screening app
//            } else {
//                // Your app is not the call screening app
//            }
//        }
//    }
}
