package com.valdemar.fwdop_a9_Pie;

import android.net.Uri;

interface TelephonyHelper {

    enum State
    {
        IDLE,
        RINGING,
        IN_CALL,
        REMOTE_OFFHOOK
    }

    public static class Stats
    {
        long calledSession;
        long calledLifetime;
    }

    Stats getStats();

    // End call
    boolean endCall();

//    void silenceRinger();

    // Place a call to the specified number.
    boolean call(Uri address);

    boolean isOK();

    State state();


    // Check if we are in either an active or holding cal
//    boolean isOffhook ();

    // Check if an incoming phone call is ringing or call waiting
//    boolean isRinging ();
}
