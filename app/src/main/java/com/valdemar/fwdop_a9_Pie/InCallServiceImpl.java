package com.valdemar.fwdop_a9_Pie;

import android.telecom.Call;
import android.telecom.InCallService;

public class InCallServiceImpl extends InCallService {

    @Override
    public void onCallAdded(Call call) {
        System.out.println("fwd: onCallAdded " + call.toString());
    }

    @Override
    public void onCallRemoved(Call call) {
        System.out.println("fwd: onCallRemoved " + call.toString());
    }
}

//public class CallService extends InCallService {
//
//    OngoingCallObject ongoingCallObject;
//
//    @Override
//    public void onCallAdded(Call call) {
//        super.onCallAdded(call);
//        new OngoingCallObject().setCall(call);
//
//        //Intent CallAct = new Intent(this, CallActivity.class);
//        //startActivity(CallAct);
//
//        CallActivity.start(this, call);
//    }
//
//    @Override
//    public void onCallRemoved(Call call) {
//        super.onCallRemoved(call);
//        new OngoingCallObject().setCall(null);
//    }
//
//}
