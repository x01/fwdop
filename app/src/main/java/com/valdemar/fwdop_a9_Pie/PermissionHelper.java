package com.valdemar.fwdop_a9_Pie;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.telecom.TelecomManager;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class PermissionHelper {

    public enum State {
        Uninitiated,
        Ongoing,
        Granted,
        MixedGranted,
        Prohibited
    }

    AppCompatActivity m_activity;
    MainActivity.Logger m_logger;
    State m_state = State.Uninitiated;

    static final int ANSWER = 0;
    static final int READ_STATE = 1;
    static final int CALL = 2;
    static final int INTERNET = 3;
    String[] m_permIds = new String[] {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ANSWER_PHONE_CALLS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.INTERNET
    };
    int[] m_perm = { -1, -1, -1, -1 };

    private static String shorten(String s) {
        return s.substring(s.lastIndexOf(".") + 1);
    }

    public PermissionHelper(AppCompatActivity act, MainActivity.Logger logger) {
        m_logger = logger;
        m_activity = act;
    }
    public State getState() { return m_state; }
    public boolean gotResult() { return m_state != State.Uninitiated && m_state != State.Ongoing; }

    private boolean hasEqual(int[] values, int val) {
        for (int i : values)
            if (i == val)
                return true;
        return false;
    }

    public void _onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        m_logger.addLog("onRequestPermissionsResult code:" + requestCode + "\n");
        boolean allGranted = true;
        boolean noneGranted = true;
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                for (int i = 0; i < permissions.length; ++i) {
                    String pid = permissions[i];
                    m_logger.addLog(shorten(pid) + ": " + grantResults[i] + "\n");
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        noneGranted = false;
                        m_perm[i] = 1;
                    } else {
                        allGranted = false;
                        m_perm[i] = 0;
                        m_logger.addLog(pid + " was not granted. Program will fail.\n");
                    }
                }
        }
        if (allGranted) m_state=State.Granted;
        else if (noneGranted) m_state=State.Prohibited;
        else m_state=State.MixedGranted;
    }

    void showRationaleDialog() {
        m_logger.addLog("Need CALL PHONE, READ PHONE STATE AND ANSWER PHONE CALLS permissions to proceed.\n");

        new AlertDialog.Builder(m_activity.getApplicationContext())
                .setTitle("Grant Permissions")
                .setMessage("FwdOp needs permissions to Call Phone, Read Phone State and Answer Calls. It can't operate without being able to do so")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_dialer)
                .show();

        m_logger.addLog("Dialog shown\n");
    }

    public void acquire() {

        m_logger.addLog("Requesting permission\n");
        boolean missing = false;
        for (String s : m_permIds) {
            int res = m_activity.checkSelfPermission(s);
            m_logger.addLog(shorten(s) + ": " + (res == PackageManager.PERMISSION_GRANTED ? "GRANTED" : "DENIED") + "\n");
            if (m_activity.checkSelfPermission(s) == PackageManager.PERMISSION_DENIED) {
                if (m_state == State.Uninitiated) m_state = State.Ongoing;
                if (m_activity.shouldShowRequestPermissionRationale(s))
                    showRationaleDialog();
                missing = true;
                m_logger.addLog("missing " + shorten(s) + "\n");
            }
        }

        if (missing) {
            m_logger.addLog("Requesting all permissions, system dialog should appear\n");
            m_activity.requestPermissions(m_permIds, 1);
        } else {
            if (m_state != State.Granted) {
                m_logger.addLog("Seems all permissions are already granted.\n");
                m_state = State.Granted;
            }
        }

        //m_logger.addLog("After request permission.\n");
    }

    public boolean check(int permission) {
        if (m_activity.checkSelfPermission(m_permIds[permission]) != PackageManager.PERMISSION_GRANTED) {
//            m_logger.addLog("Permission " + m_permIds[permission] + " not granted. Grant it in system preferences.\n");
            return false;
        }
        return true;
    }

    public boolean changeDialer() {
        m_logger.addLog("Changing dialer to " + m_activity.getPackageName() + "\n");
        Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
        intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
                m_activity.getPackageName());
        m_activity.startActivity(intent);
        return true;
    }

}
