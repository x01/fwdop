package com.valdemar.fwdop_a9_Pie;

import java.math.BigInteger;

public class AccessCode {
    static String m_xform="0123456789abcdefghijklmnpqrstuvwxyz";
    static void init(long p, long x, long bits) {
        m_p=p;
        m_x=x;
        m_bits=bits;
    }
    static long m_x=0;
    static long m_p=1;
    static long m_bits=1;
    static long to_num(String s) {
        long res=0;
        long rad=m_xform.length();
        long m=0;
        try {
            for (int i = s.length(); i-- > 0; ) {
                int digit=m_xform.indexOf(s.charAt(i));
                res += (long)Math.pow(rad, m) * digit;
                m++;
            }
        } catch(Exception e) {
            return 0;
        }
        return res;
    }

    static boolean test_num(long n) {
        long mask=(1<<m_bits)-1;
        return ((n ^ (m_x & mask)) % m_p) == 0;
    }

    static boolean is_valid(String accesscode) {
        long n=to_num(accesscode);
        return test_num(n);
    }
}
